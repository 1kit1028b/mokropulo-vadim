package ua.khpi.oop.mokropulo04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Helper {

	public static void wordCounter(List<String> stringList) {
		Map<String, Integer> stringIntegerMap = new HashMap<>(); // ������� ����� ��� �����
		for (String s : stringList) {
			stringIntegerMap.put(s, Collections.frequency(stringList, s)); // ������������ ���������� ����� ������ �����
																			// � ������
		}
		System.out.println(stringIntegerMap);

	}

	public static List<String> tokenize(String args) {
		List<String> stringList = new ArrayList<>();
		final int capacity = 50;
		StringBuilder stringBuilder = new StringBuilder(capacity);
		for (int i = 0; i < args.length(); i++) {
			if (args.charAt(i) != ' ') {
				stringBuilder.append(args.charAt(i));
			} else {
				stringList.add(stringBuilder.toString());
				stringBuilder.replace(0, stringBuilder.length(), "");
			}
		}
		stringList.add(stringBuilder.toString());
		return stringList;
	}
}
