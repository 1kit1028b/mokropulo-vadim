package ua.khpi.oop.mokropulo04;

import java.util.Scanner;

import java.util.Arrays;

public class lab4 {
	public static void main(String[] args) {
		boolean contains1 = Arrays.asList(args).contains("help");
		boolean contains2 = Arrays.asList(args).contains("debug");

		if (contains1 == true) {
			System.out.println("����������� ������ �4\r\n"
					+ "  ����: ������������ ��������� �������� ��� ��������� Java SE\r\n" + "  \r\n"
					+ "  ����: ��������� ���������� ������ ������ � ������������ � ���������� ��������� ����� Java.\r\n"
					+ "  ������: \r\n"
					+ "  1) �������������� �������� ������ �������� ����������� ������ �3, �������� �� ��������� ������ ����������� ������� ������ ����������� � ������ ���������� ����:\r\n"
					+ "  �������� �����;\r\n" + "  �������� �����;\r\n" + "  ��������� ���������;\r\n"
					+ "  ����������� ����������;\r\n" + "  ���������� �������� � �.�.\r\n"
					+ "  2) ����������� ������� ��������� ���������� ����� ��� ���������� ������ ������ ��������:\r\n"
					+ "  �������� �-h� �� �-help�: ������������ ���������� ��� ������ ��������, ����������� (������������ ��������), ��������� ���� ������ ������ (������ ���� �� ��������� ���������� �����);\r\n"
					+ "  �������� �-d� �� �-debug�: � ������ ������ �������� ������������� �������� ����, �� ���������� ������������ �� �������� ������������� ��������: ����������� �����������, ������� �������� ������, �������� ���������� ������ �� ��.\n"
					+ "  �����: ��������� �����\n");
		}

		if (contains2 == true) {
			System.out.println("��������� �������� � ������ debug");
		}
		Scanner in = new Scanner(System.in);
		System.out.println("������� ��������: ");
		System.out.println("1. ������� ���������� ���������� ���� � ������");
		System.out.println("2. ������� ���������� �������� � ������");
		System.out.println("3. ����� �� ���������");

		while (true) {
			int variable = in.nextInt();
			switch (variable) {
			case 2:
				Scanner in3 = new Scanner(System.in);
				System.out.println("������� ���������� �������� � ������");
				System.out.println("������� ���� ������:");
				String string1 = in3.nextLine();
				int sizeSpace = string1.replaceAll("[^ ]", "").length();
				int sizeSymb = string1.length();
				int size = sizeSymb - sizeSpace;
				System.out.printf("���� ������: ");
				System.out.printf(string1 + "\n");
				System.out.printf("����� ������: ");
				System.out.printf(size + "\n");
				System.out.println("���������� �������� ����������");
				break;

			case 3:
				System.out.println("��������� ���������");
				System.exit(0);
				break;
			case 1:
				Scanner in2 = new Scanner(System.in);
				System.out.println("������� ���������� ������� ����� � ������");
				System.out.println("������� ���� ������:");
				String string = in2.nextLine();
				System.out.printf("���� ������: ");
				System.out.printf(string + "\n");

				System.out.println("���������� ������� ����� � ����� ������: ");
				Helper.wordCounter(Helper.tokenize(string));
				System.out.println("C���� ����������");
				break;

			}
		}
	}

}
