package ua.khpi.oop.mokropulo06;
import java.util.Scanner;

import ua.khpi.oop.mokropulo05.Container;

public class Menu extends MenuHandler{
	
	public void launch(Container obj) {
		startMenu(obj);
	}
	
	private void startMenu(Container obj) {
		Scanner in = new Scanner(System.in);
		int choice = 0;
		do {
			System.out.println("Choose menu item: \n" 
					+ "1 - Add element\n"
					+ "2 - Clean data\n"	
					+ "3 - Show data\n"
					+ "4 - Save or retrieve data\n"
					+ "5 - Invoke 'letters' method from foreign '.class'\n"
					+ "6 - Demonstrate sequential elements handling with native and foreign helper\n"
					+ "7 - Sort elements\n"
					+ "8 - Search data\n"
					+ "9 - Compare data\n"
					+ "10- Selective processing elements with native and foreign helper\n"
					+ "0 - Exit\n");
			choice = in.nextInt();
			
			switch(choice) {
				case 1: {
					System.out.println("Write a string");
					obj.scanInfo();
					break;
				}
				
				case 2: {
					obj.clear();
					System.out.println("All data was deleted!");
					break;
				}
				
				case 3: {
					System.out.println("Data: ");
					System.out.println(obj.toString());
					break;
				}
				
				case 4: {
					int saveRetrieve = 0;
					
					System.out.println("Do you want to save(1) or retrieve(2) data?\n"
							+ "Write a number");
					System.out.println("Warning!\n"
							+ "If you do not save the current data, they will be lost!");
					
					saveRetrieve = in.nextInt();
					switch(saveRetrieve) {
						case 1: {
							System.out.println("Enter a name or file path:");
							String path = scanLine();
							saveCase(obj, path);
							System.out.println("Data was saved to file: " + path);
							break;
						}
						
						case 2: {
							System.out.println("Enter a name or file path:");
							String path = scanLine();
							obj = (Container)retrieveCase(obj, path);
							System.out.println("Data was retrieved from file: " + path);
							break;
						}
					}
					break;
				}
				
				case 5: {
					System.out.println("Enter a string to count letters:");
					String input = scanLine();
					int i = 0;
					System.out.println("In " + input + " word " + ua.khpi.oop.poplavskiy03.Helper.letters(input, i) + " letters");
					break;
				}
				
				case 6: {
					System.out.println("Enter a mode of proccessing data:\n"
							+ "0 - native\n"
							+ "1 - foreign");
					int nativeForeign = 0;
					nativeForeign = in.nextInt();
					sequentialProcessAllElements(obj, nativeForeign);
					break;
				}
				
				case 7: {
					obj.sort();
					System.out.println("Elements were sorted");
					break;
				}
				
				case 8: {
					int mode = 0;
					
					System.out.println("Do you want to search data by String(1) or index(2)?\n"
							+ "Write a number");
					
					mode = in.nextInt();
					switch(mode) {
						case 1: {
							System.out.println("Write string");
							String data = scanLine();
							searchData(obj, 0, data, 0);
							break;
						}
						
						case 2: {
							System.out.println("Enter index");
							int index = in.nextInt();
							searchData(obj, 1, "", index);
							break;
						}
					}
					break;
				}
				
				case 9: {
					subMenuForDataComparing(obj);
					break;
				}
				
				case 10: {
					System.out.println("Enter a mode of proccessing data:\n"
							+ "0 - native\n"
							+ "1 - foreign");
					int nativeForeign = 0;
					nativeForeign = in.nextInt();
					selectiveProccessing(obj, nativeForeign);
					break;
				}
				
				case 0: {
					System.out.println("Closing program...");
					in.close();
					break;
				}
				
			}
			
		} while(choice != 0);
	}	
}
