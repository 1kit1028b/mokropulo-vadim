package ua.khpi.oop.mokropulo06;

import ua.khpi.oop.mokropulo05.Container;

public class Lab6 {

	public static void main(String[] args) {
		Container firstContainer = new Container();
        Menu menu = new Menu();
        menu.launch(firstContainer);
	}

}
