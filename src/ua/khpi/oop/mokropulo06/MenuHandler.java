package ua.khpi.oop.mokropulo06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import ua.khpi.oop.mokropulo03.Helper;
import ua.khpi.oop.mokropulo05.Container;

class MenuHandler {
	protected void saveCase(Container obj, String pathToFile) {
		SerializeHandler serializeHeandler = new SerializeHandler();
		serializeHeandler.serializeObject(obj, pathToFile);
	}
	
	protected Object retrieveCase(Object obj, String pathToFile) {
		SerializeHandler serializeHeandler = new SerializeHandler();
		obj = serializeHeandler.deserializeObject(pathToFile);
		return obj;
	}
	
	protected String scanLine() {
		Scanner in = new Scanner(System.in);
		String path = in.nextLine();
		//in.close();
		return path;
	}
	
	
	/**
	 * Mode 0 - native helper, Mode 1 - foreign  helper;
	 */
	protected void sequentialProcessAllElements(Container obj, int mode) {
		if(mode == 1) {
			foreignProccessing(obj);
			return;
		} else if (mode == 0){
			nativeProccessing(obj);
			return;
		}
		System.out.println("Invalide mode, it can be 0 or 1");
	}
	
	protected void foreignProccessing(Container obj) {
		final int capacity = 100;
		StringBuilder strBuilder = new StringBuilder(capacity);
		String info = obj.toString();
		for(int i = 0; i < info.length(); i++) {
			if(info.charAt(i) != '\n') {
				strBuilder.append(info.charAt(i));
			} else {
				int c = 0;
				System.out.println(
						"In " 
				        + strBuilder.toString() 
						+ " word " 
						+ ua.khpi.oop.poplavskiy03.Helper.letters(strBuilder.toString(), c) + 
						" letters");
				strBuilder.replace(0, strBuilder.toString().length(),"");
			}
		}
	}
	
	protected void nativeProccessing(Container obj) {
		List<String> stringList = new ArrayList<>();
		stringList = Helper.tokenize(obj.toString(), '\n');
		Helper.wordCounter(stringList);
//		System.out.println(stringList);
	}
	
	protected void searchData(Container obj, int mode, String data, int index) {
		if(mode == 0) {
			int res = 0;
			res = searchByString(obj, data);
			System.out.println("Index of " + data + ": " + res);
			return;
		} else if (mode == 1) {
			String res = searchByIndex(obj, index);
			System.out.println("Elem by index " + index + " is " + res);
			return;
		}
		System.out.println("Invalide mode, it can be 0 or 1");
	}
	
	protected int searchByString(Container obj, String data) {
		return obj.getIndexOfElem(data);
	}
	
	protected String searchByIndex(Container obj, int index) {
		return obj.getElemByIndex(index);
	}
	
	protected void subMenuForDataComparing(Container obj) {
		Scanner in = new Scanner(System.in);
		List<String> stringList = new ArrayList<>();
		System.out.println("When you done enter 0");
		do {
			System.out.println("Enter string:");
			String data = in.nextLine();
			if(data.equals("0")) {
				break;
			}
			stringList.add(data);
			
		} while(true);
		String[] data = Arrays.copyOf(stringList.toArray(), stringList.toArray().length, String[].class);
		if(obj.equals(data)) {
			System.out.println("Arrays are equals!");
		} else {
			System.out.println("Arrays not equals!");
		}
	}
	
	protected void selectiveProccessing(Container obj, int mode) {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter index of element to proccessing");
		int index = in.nextInt();
		String res = obj.getElemByIndex(index);
		if(res.equals("")) {
			System.out.println("\nNo string fo such index");
			return;
		}
		if(mode == 1) {
			selectiveForeignProccessing(obj, res);
		} else if (mode == 0) {
			selectiveNativeProccessing(obj, res);
		}
	}
	
	protected void selectiveForeignProccessing(Container obj, String str) {
		int c = 0;
		System.out.println(
				"In " 
		        + str
				+ " word " 
				+ ua.khpi.oop.poplavskiy03.Helper.letters(str, c) + 
				" letters");
	}
	
	protected void selectiveNativeProccessing(Container obj, String str) {
		List<String> stringList = new ArrayList<>();
		stringList.add(str);
		Helper.wordCounter(stringList);
	}

}
