package ua.khpi.oop.mokropulo01;

public class Lab1 {
	public static void main(String[] args) {
		int bookNumber = 0X0467B; // 18043
		long phoneNumber = 380665427539l;
		int last2PhoneNumb = 0b100111; // 39
		int last4PhoneNumb = 7537;
		double rest = 1.16;
		char symbhol = 'a';

		long BookNumberEven = even(bookNumber);
		long BookNumberOdd = odd(bookNumber);
		long PhoneNumberEven = even(phoneNumber);
		long PhoneNumberOdd = odd(phoneNumber);
		long Last2PhoneNumbEven = even(last2PhoneNumb);
		long Last2PhoneNumbODd = odd(last2PhoneNumb);
		long Last4PhoneNumbEvenEven = (last4PhoneNumb);
		long Last2PhoneNumbOdd = odd(last4PhoneNumb);

		countBinaryOnes(bookNumber);
		countBinaryZero(bookNumber);
		countBinaryOnes(phoneNumber);
		countBinaryZero(phoneNumber);
		countBinaryOnes(last2PhoneNumb);
		countBinaryZero(last2PhoneNumb);
		countBinaryOnes(last4PhoneNumb);
		countBinaryZero(last4PhoneNumb);

		// ������� 2
		/*
		 * System.out.print("������� 2: "); System.out.print(Even(BookNumber));
		 * System.out.print(Odd(BookNumber)); System.out.print(Even(PhoneNumber));
		 * System.out.print(Odd(PhoneNumber)); System.out.print(Even(Last2PhoneNumb));
		 * System.out.print(Odd(Last2PhoneNumb));
		 * System.out.print(Even(Last4PhoneNumb));
		 * System.out.print(Odd(Last4PhoneNumb));
		 * 
		 * System.out.print(" ������� 3: ");
		 * System.out.print(countBinaryOnes(BookNumber));
		 * System.out.print(countBinaryZero(BookNumber));
		 * System.out.print(countBinaryOnes(PhoneNumber));
		 * System.out.print(countBinaryZero(PhoneNumber));
		 * System.out.print(countBinaryOnes(Last2PhoneNumb));
		 * System.out.print(countBinaryZero(Last2PhoneNumb));
		 * System.out.print(countBinaryOnes(Last4PhoneNumb));
		 * System.out.print(countBinaryZero(Last4PhoneNumb));
		 * 
		 * System.out.print(" �����");
		 */
	}

	static long even(long x) {
		long b;
		int count1 = 0;
		do {
			b = x % 10;
			if (b % 2 == 0) {
				count1++;
			}
			x /= 10;
		} while (x != 0);
		return count1;
	}

	static long odd(long x) {
		long b;
		int count2 = 0;
		do {
			b = x % 10;
			if (b % 2 != 0) {
				count2++;
			}
			x /= 10;
		} while (x != 0);
		return count2;
	}

	public static long countBinaryOnes(long number) {
		final char ONE = '1';
		return Long.toBinaryString(number).chars().filter(item -> item == ONE).count();
	}

	public static long countBinaryZero(long number) {
		final char ONE = '0';
		return Long.toBinaryString(number).chars().filter(item -> item == ONE).count();
	}
}
