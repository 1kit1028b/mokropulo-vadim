package ua.khpi.oop.mokropulo07;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Scanner;


public class Interface {

	public static void printMenu() {
		System.out.println("Возможные варианты работы с данными: \n"
		+"1. Добавить нового клиента \n"
	    +"2. Удалить клиента \n"
		+"3. Очистить список клиентов \n"
		+"4. Показать список \n"
		+"5. Выход \n");
	}
	
	public static void cls() {
		for(int i = 0; i < 100; i++) {
			System.out.println();
		}
	}
	
	public static void printInputInterface() {
		System.out.println("1. Ввести номер паспорта \n"
		+"2. Ввести фамилию \n"
		+"3. Ввести имя \n"
		+"4. Ввести отчество \n"
		+"5. Ввести дату заселения \n"
		+"6. Ввести дату выселения \n"
		+"7. Ввести номер помещения \n"
		+"8. Ввести класс номера \n"
		+"9. Ввести количество мест в номере \n"
		+"10. Ввести причину заселения \n"
		+"11. Показать введённые данные \n"
		+"12. Подтвердить ввод и добавить клиента \n"
		+"0. Выход \n");
		
	}
	
	
	public static  int inInt() {
		System.out.println("Ожидание ввода числа");
		@SuppressWarnings("resource")
		int user_int = new Scanner(System.in).nextInt();
		return user_int;
	}
	
	 public  static String inStr() {
		 System.out.println("Ожидание ввода строки");
			@SuppressWarnings("resource")
			String user_str = new Scanner(System.in).nextLine();
			return user_str;
		}
	 
	 public static GregorianCalendar inCalendar() {
		 System.out.println("Введите год");
		 int year = inInt();
		 System.out.println("Введите месяц");
		 int month = inInt();
		 System.out.println("Введите день");
		 int day = inInt();
		return new GregorianCalendar(year,month-1,day);
		 
		 
	 }


		public static void showData(int passportNumber,
				String surname,
				String name,
				String patronymic,
				GregorianCalendar settlementDate,
				GregorianCalendar evictionDate,
				int numberOfRoom,
				String classOfRoom,
				int countOfPlaces,
				ArrayList<String> settlementReason) {
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("|  Номер паспорта  |       Фамилия       |        Имя        |        Отчество        |              Дата заселения              |              Дата выселения              |   Номер помещения   |       Класс       |   Количество мест   |");
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			System.out.printf("|    %9d     |   %10s        |  %10s       |      %10s        |",passportNumber,surname,name,patronymic);
			System.out.print("       "+ settlementDate.getTime() + "       |       " + evictionDate.getTime() + "       |");
			System.out.printf("    %8d         |   %10s      |    %8d         |\n",numberOfRoom ,classOfRoom ,countOfPlaces);
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Причины заселения");
			int i = 1;
			for(String s: settlementReason) {
				System.out.println((i++) + ". " + s);
			}
			
		}
		
	public static Hotel create(int passportNumber,
			String surname,
			String name,
			String patronymic,
			GregorianCalendar settlementDate,
			GregorianCalendar evictionDate,
			int numberOfRoom,
			String classOfRoom,
			int countOfPlaces,
			ArrayList<String> settlementReason) {
		
				return new Hotel(passportNumber,surname,name,patronymic,settlementDate,evictionDate,new HotelRoom(numberOfRoom,classOfRoom,countOfPlaces),settlementReason);
		
	}
	
	public static void showArray(ArrayList<Hotel> input) {
		int i = 1;
		for(Hotel s: input) {
			System.out.println("///////////////////////////////" + (i++) + "\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\");
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("|  Номер паспорта  |       Фамилия       |        Имя        |        Отчество        |              Дата заселения              |              Дата выселения              |   Номер помещения   |       Класс       |   Количество мест   |");
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			System.out.printf("|    %9d     |   %10s        |  %10s       |      %10s        |",s.getPassportNumber(),s.getSurname() ,s.getName() ,s.getPatronymic());
			System.out.print("       "+ s.getSettlementDate().getTime() + "       |       " + s.getEvictionDate().getTime() + "       |");
			System.out.printf("    %8d         |   %10s      |    %8d         |\n",s.hotelRoom.getNumberOfRoom() , s.hotelRoom.getClassOfRoom() , s.hotelRoom.getCountOfPlaces());
			System.out.println("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
			System.out.println("Причины заселения");
			int y = 1;
			for(String s1: s.settlementReason) {
				System.out.println((y++) + ". " + s1);
			}
			System.out.println("\n\n\n");
			
		}
	}
}
